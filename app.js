const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const logger = require('morgan');
const FileStore = require('session-file-store')(session);
const fs = require('fs');
const http = require('http');
//const https = require('https');

const Common = require('./classes/util/Common');
const ViewTemplate = require('./classes/util/ViewTemplate');

const compileSass = require('compile-sass');

const server_conf = JSON.parse(fs.readFileSync('./server_conf.json'));
console.log(server_conf);

const config = {
	isDebug: true,
	logOnStart: false,
	port: {
		http: 4000,
		// https: 443
	},
	session: {
		ttl: 24 * 60 * 60, // 1일
		secret: "secret"
	},
	deleteExpiredSessionFile: true
};

Common.rootDir = __dirname;
Common.isDebug = config.isDebug;
Common.isLive(server_conf.isLive);
Common.setDateFormatFunction();
Common.setNumberToStringFunction();
Common.setAddCommaToNumberFunction();

if (Common.logOnStart) console.log('start server');

const app = express();

if (Common.logOnStart) console.log('setup view engine');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/css/:cssName', compileSass.setup({
  sassFilePath: path.join(__dirname, 'public/scss/'),
  sassFileExt: 'sass',
  embedSrcMapInProd: true,
  resolveTildes: true,
  nodeSassOptions: {
    errLogToConsole: true,
    noCache: true,
    force: true
  }
}));

app.use(session({
	store: new FileStore({
		ttl: config.session.ttl,
		fileExtension: '',
		encoder: value => Buffer.from(JSON.stringify(value)).toString('base64'),
		decoder: value => JSON.parse(Buffer.from(value, 'base64').toString())
	}),
    secret: config.session.secret,
    cookie: {
        maxAge: config.session.ttl * 1000
    },
    saveUninitialized: false,
    resave: false
}));

app.use(express.static(path.join(__dirname, 'public')));

if (Common.logOnStart) console.log('setup routers');
setRouters('/', './routes/page');

function setRouters(urlPrefix, dir) {
	const fileList = fs.readdirSync(dir);
	for (let i = 0; i < fileList.length; i++) {
		const url = (urlPrefix + fileList[i]).replace('root.js', '').replace('.js', '');
		const file = dir + '/' + fileList[i];
		//console.log('set router', file, 'to', url);
		app.use(url, require(file));
	}
}

if (Common.logOnStart) console.log('setup 404 error handler');
app.use((req, res, next) => ViewTemplate.error404(req, res));

if (Common.logOnStart) console.log('setup 500 error handler');
app.use((err, req, res) => ViewTemplate.error500(req, res, err));

if (Common.logOnStart) console.log('setup exception handler');
app.use((err, req, res, next) => ViewTemplate.error500(req, res, err));

// if (config.makeDirectoriesIfNotExist) {
// 	if (Common.logOnStart) console.log('make directories if not exist');
// 	if (!fs.existsSync('public/uploads'))
// 		fs.mkdirSync('public/uploads', { mode: 777 });
// 	if (!fs.existsSync('public/uploads_thumb'))
// 		fs.mkdirSync('public/uploads_thumb', { mode: 777 });
// 	if (!fs.existsSync('sessions'))
// 		fs.mkdirSync('sessions', { mode: 777 });
// }

if (config.deleteExpiredSessionFile) {
	if (Common.logOnStart) console.log('delete expired session file');
	const sessions = fs.readdirSync('sessions');
	const current_date = new Date();
	for (let i = 0; i < sessions.length; i++) {
		const file = sessions[i];
		const session = JSON.parse(Buffer.from(fs.readFileSync('sessions/' + file).toString(), 'base64').toString());
		const expires = new Date(session.cookie.expires);
		if (+current_date > +expires) fs.unlinkSync('sessions/' + file);
	}
}

if (Common.logOnStart) console.log('create and listen http server, port : ' + config.port.http);
const http_server = http.createServer(app);
http_server.listen(config.port.http);
http_server.on('error', onError(config.port.http));
http_server.on('listening', onListening(http_server));

// if (Common.logOnStart) console.log('create and listen https server, port : ' + config.port.https);
// const https_option = {
// 	key: fs.readFileSync(config.ssl.key),
// 	cert: fs.readFileSync(config.ssl.cert)
// };
// const https_server = https.createServer(https_option, app);
// https_server.listen(config.port.https);
// https_server.on('error', onError(config.port.https));
// https_server.on('listening', onListening(https_server));

if (Common.logOnStart) console.log('musesalon server is started at ' + new Date().format("yyyy-MM-dd HH:mm:ss"));

function forwardProtocolTo(protocol) {
	const port_from = protocol === 'http' ? config.port.https : config.port.http;
	const port_to = protocol === 'http' ? config.port.http : config.port.https;
	return (req, res) => {
		res.writeHead(301, { "Location": protocol + "://" + req.headers['host'].replace(port_from, port_to) + req.url });
		res.end();
	};
}

function onError(port) {
	return error => {
		if (error.syscall !== 'listen') {
			throw error;
		}

		const bind = typeof port === 'string'
			? 'Pipe ' + port
			: 'Port ' + port;

		// handle specific listen errors with friendly messages
		switch (error.code) {
			case 'EACCES':
				console.error(bind + ' requires elevated privileges');
				process.exit(1);
				break;
			case 'EADDRINUSE':
				console.error(bind + ' is already in use');
				process.exit(1);
				break;
			default:
				throw error;
		}
	};
}

function onListening(server) {
	return () => {
		const addr = server.address();
		const bind = typeof addr === 'string'
			? 'pipe ' + addr
			: 'port ' + addr.port;
		console.log('Listening on ' + bind);
	};
}
