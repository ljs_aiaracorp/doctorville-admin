class Admin {
  adminId = 0; // 어드민 아이디
  iso = ""; // 관리자 국가 ISO 정보
  name = ""; // 관리자 이름
  mobileCode = ""; // 관리자 모바일 국가 코드
  mobile = ""; // 관리자 모바일 번호
  isDisable = false; // 관리자 비활성화 여부
  ip = ""; // 관리자 가입 ip
  createdAt = ""; // 관리자 생성 시간
  updatedAt = ""; // 관리자 수정 시간
  deletedAt = ""; // 관리자 삭제 시간
  accessToken = ""; // 인증 토큰
  accessTokenCurrentAt = ""; // 인증 토큰 생성 시간
  accessTokenExpireAt = ""; // 인증 토큰 만료 시간
  scope = []; // 인증 권한 범위
  refreshToken = ""; // 인증 토큰 갱신용 토큰
  refreshTokenExpireAt = ""; // 갱신용 토큰 만료 시간

  constructor(json) {
    this.adminId = json.adminId;
    this.iso = json.iso;
    this.name = json.name;
    this.mobileCode = json.mobileCode;
    this.mobile = json.mobile;
    this.isDisable = json.isDisable;
    this.ip = json.ip;
    this.createdAt = json.createdAt;
    this.updatedAt = json.updatedAt;
    this.deletedAt = json.deletedAt;
    this.accessToken = json.accessToken;
    this.accessTokenCurrentAt = json.accessTokenCurrentAt;
    this.accessTokenExpireAt = json.accessTokenExpireAt;
    this.scope = json.scope;
    this.refreshToken = json.refreshToken;
    this.refreshTokenExpireAt = json.refreshTokenExpireAt;
  }
}

module.exports = Admin;