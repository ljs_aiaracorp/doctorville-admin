class User {
  userId = 0; // 유저 아이디
  iso = ""; // 유저 국가 ISO 정보
  name = ""; // 유저 이름
  nickname = ""; // 유저 별명
  mobileCode = ""; // 유저 모바일 국가 코드
  mobile = ""; // 유저 모바일 번호
  type = ""; // 유저 의사 유형 doctor, specialist, resident, fellow, etc
  licenseNumber = ""; // 유저 의사면허 번호
  hospital = ""; // 유저 소속 병원
  department = ""; // 유저 진료 영역
  memo = ""; // 유저 메모
  isReceiveEmail = false; // 유저 이메일 수신 여부
  isReceiveSms = false; // 유저 문자 수신 여부
  isPrivacyInfomation = false; // 유저 개인정보활용동의 여부
  isDisable = false; // 유저 비활성화 여부
  isDelete = false; // 유저 탈퇴 여부
  ip = ""; // 유저 가입 ip
  createdAt = ""; // 인증 정보 생성 시간
  updatedAt = ""; // 인증 정보 수정 시간
  deletedAt = ""; // 유저 탈퇴 시간
  accessToken = ""; // 인증 토큰
  accessTokenCurrentAt = ""; // 인증 토큰 생성 시간
  accessTokenExpireAt = ""; // 인증 토큰 만료 시간
  scope = []; // 인증 권한 범위
  refreshToken = ""; // 인증 토큰 갱신용 토큰
  refreshTokenExpireAt = ""; // 갱신용 토큰 만료 시간

  constructor(json) {
    this.userId = json.userId;
    this.iso = json.iso;
    this.name = json.name;
    this.nickname = json.nickname;
    this.mobileCode = json.mobileCode;
    this.mobile = json.mobile;
    this.type = json.type;
    this.licenseNumber = json.licenseNumber;
    this.hospital = json.hospital;
    this.department = json.department;
    this.memo = json.memo;
    this.isReceiveEmail = json.isReceiveEmail;
    this.isReceiveSms = json.isReceiveSms;
    this.isPrivacyInfomation = json.isPrivacyInfomation;
    this.isDisable = json.isDisable;
    this.isDelete = json.isDelete;
    this.ip = json.ip;
    this.createdAt = json.createdAt;
    this.updatedAt = json.updatedAt;
    this.deletedAt = json.deletedAt;
    this.accessToken = json.accessToken;
    this.accessTokenCurrentAt = json.accessTokenCurrentAt;
    this.accessTokenExpireAt = json.accessTokenExpireAt;
    this.scope = json.scope;
    this.refreshToken = json.refreshToken;
    this.refreshTokenExpireAt = json.refreshTokenExpireAt;
  }
}

module.exports = User;