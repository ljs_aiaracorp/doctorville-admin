class AjaxHelper {
	static success(res, result) {
		if (result === undefined) result = 'success';
		if (typeof result === 'number') result += '';
		res.status(200).send(result).end();
	}
	static badRequest(res, msg) {
		if (msg === undefined) msg = 'bad request';
		res.status(400).send(msg).end();
	}
	static unauthorized(res, msg) {
		if (msg === undefined) msg = 'unauthorized';
		res.status(401).send(msg).end();
	}
	static notFound(res, msg) {
		if (msg === undefined) msg = 'not found';
		res.status(404).send(msg).end();
	}
	static error(res, msg) {
		if (msg === undefined) msg = 'error';
		console.trace();
		res.status(500).send(msg).end();
	}

	static status(res, status, msg) {
		switch (status) {
			case 400: this.badRequest(res, msg); break;
			case 401: this.unauthorized(res, msg); break;
			case 404: this.notFound(res, msg); break;
			default: this.error(res, msg); break;
		}
	}
}

module.exports = AjaxHelper;
