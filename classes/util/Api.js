const fetch = require('node-fetch');
const { URLSearchParams } = require('url');
const FormData = require('form-data');
const fs = require('fs');

class Api {
  static checkLogin(req) {
    return !!req.session.admin;
  }

  static logout(access_token) { // 로그아웃
    return fetch(`${Api.END_POINT}/oauth/sessions`, {
      method: 'delete',
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
    }).then(res => res.json());
  }
  static login(identifier, key) { // 로그인
    const params = new URLSearchParams();
    params.append('type', 'email');
    params.append('identifier', identifier);
    params.append('key', key);
    // params.append('session', true);
    return fetch(`${Api.END_POINT}/admins/login`, {
      method: 'post',
      body: params
    }).then(res => res.json());
  }

  static getUsers(offset, limit, access_token) {
    return fetch(`${Api.END_POINT}/users?offset=${offset}&limit=${limit}`, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    }).then(res => res.json());
  }

  static getUser(userId, access_token) {
    return fetch(`${Api.END_POINT}/users/${userId}`, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    }).then(res => res.json());
  }
}

Api.END_POINT = '';

module.exports = Api;
