const fs = require('fs');
const URL = require('url').URL;
const Api = require('./Api');

class Common {
	static number(val) {
		if (typeof val === 'string') {
			return parseInt(val);
		}
		if (typeof val === 'number') {
			return val;
		}
		return NaN;
	}
	static setAddCommaToNumberFunction() {
		if (Number.prototype.addCommaToNumber === undefined) {
			Number.prototype.addCommaToNumber = function () {
				if (this == 0) return 0;

				var reg = /(^[+-]?\d+)(\d{3})/;
				var n = (this + '');

				while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

				return n;
			};
		}

		if (String.prototype.addCommaToNumber === undefined) {
			String.prototype.addCommaToNumber = function () {
				var num = parseFloat(this);
				if (isNaN(num)) return "0";

				return num.addCommaToNumber();
			};
		}
	}
	static setNumberToStringFunction() {
		if (Number.prototype.numberToString === undefined) {
			Number.prototype.numberToString = function (length) {
				let str = this.toString();
				if (str.length >= length) return str;
				while (str.length < length) {
					str = "0" + str;
				}
				return str;
			}
		}
	}
	static setDateFormatFunction() {
		if (Date.prototype.format === undefined) {
			Date.prototype.format = function (f) {
				if (!this.valueOf()) return " ";
				const weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
				const d = this;
				return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|ms|a\/p)/gi, function ($1) {
					switch ($1) {
						case "yyyy":
							return d.getFullYear();
						case "yy":
							return (d.getFullYear() % 100).numberToString(2);
						case "MM":
							return (d.getMonth() + 1).numberToString(2);
						case "dd":
							return d.getDate().numberToString(2);
						case "E":
							return weekName[d.getDay()];
						case "HH":
							return d.getHours().numberToString(2);
						case "hh":
							return ((d.getHours() % 12) ? d.getHours() : 12).numberToString(2);
						case "mm":
							return d.getMinutes().numberToString(2);
						case "ss":
							return d.getSeconds().numberToString(2);
						case "ms":
							return d.getMilliseconds().numberToString(3);
						case "a/p":
							return d.getHours() < 12 ? "오전" : "오후";
						default:
							return $1;
					}
				});
			}
		}
	}
	static rawDateToFrontDate(raw, format) {
		if (format === undefined) format = "yyyy.MM.dd";
		return new Date(raw.toString()).format(format);
	}
	static currentDateString(format) {
		if (format === undefined) format = "yyyy.MM.dd";
		return new Date().format(format);
	}
	static currentTimeString(format) {
		if (format === undefined) format = "HH:mm:ss";
		return new Date().format(format);
	}
	static getUrl(req, path) {
		// console.log(path);
		if (path === undefined || path === null) path = '';
		if (path.indexOf('https://') > -1) {
			return path;
		}
		if (path.indexOf('amazonaws.com') > -1) {
			return path;
		}
		if (path.indexOf(req.headers.host) < 0) {
			if (path[0] !== '/') path = '/' + path;
			path = req.headers.host + path;
		}
		if (path.indexOf('http://') < 0 && path.indexOf('https://') < 0) {
			if (this.allowHttp()) path = 'http://' + path;
			else path = 'https://' + path;
		}
		let url = new URL(path);
		// console.log(' -> ', url.href);
		return url.href;
	}
	static allowHttp() {
		return this.isLocal || this.isDebug;
	}
	static allowHttps() {
		return this.enableSsl && (!this.isLocal || this.isDebug);
	}
	static getLocalPath(req, url, includePublic) {
		if (url === undefined) url = '';
		if (includePublic === undefined) includePublic = true;
		if (includePublic)
			return 'public' + url.replace('http://', '')
								.replace('https://', '')
								.replace('musesalon.s3.ap-northeast-2.amazonaws.com', '/uploads')
								.replace('public/public', 'public')
								.replace(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):[\d]+$/, '')
								.replace(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/, '')
								.replace(/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?:[\d]+$/, '')
								.replace(/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/, '');
		else
			return url.replace('http://', '')
						.replace('https://', '')
						.replace('musesalon.s3.ap-northeast-2.amazonaws.com', '/uploads')
						.replace('public/', '')
						.replace(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):[\d]+$/, '')
						.replace(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/, '')
						.replace(/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?:[\d]+$/, '')
						.replace(/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/, '');
	}
	static deleteLocalFile(req, file) {
		if (file !== undefined && file !== null && file !== '') {
			if ((file.indexOf('http://') > -1 || file.indexOf('https://') > -1) && file.indexOf(req.headers.host) === -1)
				return; // url인데 외부 url이면 삭제 못하니 그냥 리턴
			const path = Common.getLocalPath(req, file);
			if (fs.existsSync(path))
				fs.unlinkSync(path);
		}
	}
	static makeRandomText(length) {
		const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if (length === undefined) length = 10;
		let str = '';
		for (let i = 0; i < length; i++) {
			let rnum = Math.floor(Math.random() * chars.length);
			str += chars.substring(rnum, rnum + 1);
		}
		return str;
	}

	static calculateImageSize(w, h, dst_w, dst_h) {
		if (w === h) return { w: 500, h: 500 };
		const ratio = w / h;
		let new_w, new_h;
		if (dst_w / dst_h > ratio) {
			new_w = dst_h * ratio;
			new_h = dst_h;
		} else {
			new_h = dst_w / ratio;
			new_w = dst_w;
		}
		return {
			w: new_w,
			h: new_h
		};
	}

	static isLive(isLive) {
		if (isLive) {
			Api.END_POINT = 'http://api.doctorville.aiaracorp.com';
		} else {
			Api.END_POINT = 'http://api.doctorville.aiaracorp.com';
		}
	}
}
Common.rootDir = '';
Common.isDebug = false;
Common.isLocal = false;
Common.logOnStart = false;
Common.enableSsl = false;

module.exports = Common;
