const xl = require('excel4node');

class ExcelHelper {
	download(res, filename, header, data) {
		const wb = new xl.Workbook();
		const ws = wb.addWorksheet('sheet1');
		let rIdx = 1;
		let cIdx = 1;
		for (const h of header) {
			ws.cell(rIdx, cIdx++).string(h.label);
		}
		rIdx++;
		for (const d of data) {
			cIdx = 1;
			for (const h of header) {
				ws.cell(rIdx, cIdx++).string(d[h.key]);
			}
			rIdx++;
		}
		if (!filename.endsWith('.xlsx') && !filename.endsWith('.xls')) {
			filename += '.xlsx';
		}
		wb.write(filename, res);
	}
}

module.exports = ExcelHelper;
