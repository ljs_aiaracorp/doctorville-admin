const multer = require('multer');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'public/uploads/');
	},
	filename: (req, file, cb) => {
		cb(null, (+new Date()) + "_" + file.originalname);
	}
});
const upload = multer({
	storage: storage
});

class Uploader {
	static single(file) {
		return upload.single(file);
	}
	static array(file) {
		return upload.array(file);
	}
}

module.exports = Uploader;
