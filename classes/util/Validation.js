const ApiResponse = require('./ApiResponse');

class Validation {
	static isPositive(val) {
		if (typeof val === 'string') {
			return parseInt(val) > 0;
		}
		if (typeof val === 'number') {
			return val > 0;
		}
		return false;
	}
	static checkExist(req, param_names) {
		return req.body[param_names] !== undefined && req.body[param_names] !== null && req.body[param_names] !== '';
	}
	static exist(req, res, param_names, msg) {
		if (typeof param_names === 'string') {
			if (!this.checkExist(req, param_names)) {
				ApiResponse.notFoundParameter(res, param_names, msg);
				return false;
			}
			return true;
		}
		for (let i = 0; i < param_names.length; i++)
			if (this.checkExist(req, param_names[i])) return true;
		ApiResponse.notFoundParameter(res, param_names.join(', '), msg);
		return false;
	}
	static email(req, res, param_name, msg) {
		if (!this.emailValidationRegexp.test(req.body[param_name])) {
			ApiResponse.validationFail(res, param_name, msg);
			return false;
		}
		return true;
	}
	static accountPassword(req, res, param_name, msg) {
		if (!this.accountPasswordValidationRegexp.test(req.body[param_name])) {
			ApiResponse.validationFail(res, param_name, msg);
			return false;
		}
		return true;
	}
	static positiveNum(req, res, param_name, msg) {
		if (!this.isPositive(req.body[param_name])) {
			ApiResponse.validationFail(res, param_name, msg);
			return false;
		}
		return true;
	}
	static allowed(req, res, param_name, msg, allowed) {
		for (let i = 0; i < allowed.length; i++) {
			if (req.body[param_name] === allowed[i]) {
				return true;
			}
		}
		ApiResponse.validationFail(res, param_name, msg);
		return false;
	}
	static uuid(req, res, param_name) {
		if (param_name === undefined) param_name = 'uuid';
		if (!this.isPositive(req.params[param_name])) {
			ApiResponse.validationFail(res, param_name, param_name + '는 0보다 큰 수로 입력해 주세요.');
			return false;
		}
		return true;
	}
}
Validation.emailValidationRegexp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
Validation.accountPasswordValidationRegexp = /^(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()_=\-+])[A-Za-z\d!@#$%^&*()_=\-+]{8,}$/;

module.exports = Validation;
