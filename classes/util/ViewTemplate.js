const fs = require('fs');
const ejs = require('ejs');

const AjaxHelper = require('./AjaxHelper');
const Common = require('./Common');
const Api = require('./Api');

const master = fs.readFileSync('./views/templates/master.ejs', 'utf8');
const header = fs.readFileSync('./views/templates/header.ejs', 'utf8');
const sidemenu = fs.readFileSync('./views/templates/sidemenu.ejs', 'utf8');

class ViewTemplate {
	static set(req, res, view, header_title, sidemenu_active, sidemenu_sub_active, option) {
		res.writeHead(200, { 'Content-Type': 'text/html' });
		if (view === 'login') {
			res.write(ejs.render(fs.readFileSync('./views/login.ejs', 'utf8'), option));
		} else {
			res.write(ejs.render(master, {
				header: ejs.render(header, { title: header_title, loginName: Api.checkLogin(req) ? req.session.admin.name : '' }),
				sidemenu: ejs.render(sidemenu, { active: sidemenu_active, sub_active: sidemenu_sub_active }),
				contents: ejs.render(fs.readFileSync('./views/' + view + '.ejs', 'utf8'), option),
				jsList: option.jsList ? option.jsList : [],
			}));
		}
		res.end();
	}
	static setAjaxReturn(res, view, option, currentPage, countPerPage, totalCount) {
		res.writeHead(200, { 'Content-Type': 'text/html' });
		if (currentPage !== undefined && countPerPage !== undefined && totalCount !== undefined) {
			option.pagination = ejs.render(fs.readFileSync('./views/templates/pagination.ejs', 'utf8'), {
				current_page: Common.number(currentPage),
				count_per_page: Common.number(countPerPage),
				total_count: Common.number(totalCount)
			});
			option.current_page = Common.number(currentPage);
			option.count_per_page = Common.number(countPerPage);
			option.total_count = Common.number(totalCount);
		}
		res.write(ejs.render(fs.readFileSync('./views/' + view + '.ejs', 'utf8'), option));
		res.end();
	}
	static error404(req, res) {
		if (req.url.indexOf('/ajax/') > -1) {
			AjaxHelper.notFound(res);
			return;
		}
		res.writeHead(404, {'Content-Type': 'text/html'});
		res.write(ejs.render(fs.readFileSync('./views/404.ejs', 'utf8')));
		res.end();
	}
	static error500(req, res, err) {
		console.error(err);
		if (req.url.indexOf('/ajax/') > -1) {
			AjaxHelper.error(res, err);
			return;
		}
		res.writeHead(500, {'Content-Type': 'text/html'});
		res.write(ejs.render(fs.readFileSync('./views/500.ejs', 'utf8')));
		res.end();
	}
}

module.exports = ViewTemplate;
