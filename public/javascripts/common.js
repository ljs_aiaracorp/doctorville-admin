'use strict';

const emailValidationRegexp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

class Common {
	static logout() {
		$.ajax({
			url: '/ajax/logout',
			method: 'post'
		}).then(() => {
			location.href = '/login';
		});
	}
	static checkInput($input, msg) {
		const value = $input.val();
		if (value === '') {
			alert(msg);
			$input.focus();
			return false;
		}
		return true;
	}
	static checkInputFile($input, msg) {
		const value = $input.data('path');
		if (value === '') {
			alert(msg);
			$input.focus();
			return false;
		}
		return true;
	}
	static setPaginationBtnEvent(loadPage, addClass) {
		if (addClass === undefined) {
			$(".pagination > :not(.disabled) > .page-link").off('click').on('click', function() {
				loadPage($(this).data('page'));
			});
		} else {
			$(".pagination." + addClass + " > :not(.disabled) > .page-link").off('click').on('click', function() {
				loadPage($(this).data('page'));
			});
		}
	}
	// static uploadFile(input, isMarker) {
	// 	if (isMarker === undefined) isMarker = false;
	// 	const formData = new FormData();
	// 	formData.append('file', input.files[0]);
	// 	const accept = $(input).attr('accept');
	// 	if (accept.indexOf('image') > -1) {
	// 		if (input.files[0].type !== 'image/jpeg' && input.files[0].type !== 'image/jpg' && input.files[0].type !== 'image/png') {
	// 			alert(isMarker ? "마커는 jpg, png 파일을 업로드 해주세요." : "이미지 파일은 jpg, png 파일을 업로드 해주세요.");
	// 			return;
	// 		}
	// 	}
	// 	if (accept.indexOf('video') > -1) {
	// 		if (input.files[0].type !== 'video/mp4') {
	// 			alert("영상 파일은 mp4 파일을 업로드 해주세요.");
	// 			return;
	// 		}
	// 	}
	// 	if (accept.indexOf('audio') > -1) {
	// 		if (input.files[0].type !== 'audio/mp3') {
	// 			alert("오디오 파일은 mp3 파일을 업로드 해주세요.");
	// 			return;
	// 		}
	// 	}
	// 	$.ajax({
	// 		url: isMarker ? "/ajax/marker_upload" : "/ajax/upload",
	// 		method: "post",
	// 		processData: false,
	// 		contentType: false,
	// 		data: formData
	// 	}).then(res => {
	// 		$(input).data('path', res.url);
	// 		$("label[for=" + input.id + "]").text(res.filename);
	// 		if (input.files[0].type.indexOf('video/') > -1) {
	// 			if ($(input).parent().parent().find('.preview').length > 0) {
	// 				$(input).parent().parent().find('.preview').addClass('d-none');
	// 				$(input).parent().parent().find('.preview').attr('src', '');
	// 			}
	// 			if ($(input).parent().parent().find('.preview-video').length > 0)
	// 				$(input).parent().parent().find('.preview-video').attr('src', res.url).removeClass('d-none');
	// 		}
	// 		if (input.files[0].type.indexOf('image/') > -1){
	// 			if ($(input).parent().parent().find('.preview-video').length > 0) {
	// 				$(input).parent().parent().find('.preview-video').addClass('d-none');
	// 				$(input).parent().parent().find('.preview-video').attr('src', '');
	// 			}
	// 			if ($(input).parent().parent().find('.preview').length > 0)
	// 				$(input).parent().parent().find('.preview').attr('src', res.url).removeClass('d-none');
	// 		}
	// 	}).catch(err => {
	// 		console.error(err);
	// 		if (err.responseText === '_filename_') {
	// 			alert("파일명을 영문 + 숫자로 변경하고 다시 업로드해 주세요.");
	// 			return;
	// 		}
	// 		alert("에러가 발생했습니다. " + err.responseText);
	// 	});
	// }
}

$(document).ready(() => {
	// $("#logout-btn").off('click').on('click', Common.logout);
});

Date.prototype.format = function(f) {
	if (!this.valueOf()) return " ";

	var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
	var d = this;
	 
	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
			switch ($1) {
					case "yyyy": return d.getFullYear();
					case "yy": return (d.getFullYear() % 1000).zf(2);
					case "MM": return (d.getMonth() + 1).zf(2);
					case "dd": return d.getDate().zf(2);
					case "E": return weekName[d.getDay()];
					case "HH": return d.getHours().zf(2);
					case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
					case "mm": return d.getMinutes().zf(2);
					case "ss": return d.getSeconds().zf(2);
					case "a/p": return d.getHours() < 12 ? "오전" : "오후";
					default: return $1;
			}
	});
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this === undefined ? '' : this.toString().zf(len);};

Number.prototype.addCommaToNumber = function () {
	if (this == 0) return 0;

	var reg = /(^[+-]?\d+)(\d{3})/;
	var n = (this + '');

	while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

	return n;
};
String.prototype.addCommaToNumber = function () {
	var num = parseFloat(this);
	if (isNaN(num)) return "0";

	return num.addCommaToNumber();
};
