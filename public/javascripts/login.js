class LoginPage {
  init() {
    $("#login-btn").on('click', this.login);
    $("#email").on('keyup', this.onKeyUp);
    $("#password").on('keyup', this.onKeyUp);
  }

  login() {
    $.ajax({
      url: '/ajax/login',
      method: 'post',
      data: {
        email: $("#email").val(),
        password: $("#password").val(),
        saveInfo: $("#save-info").prop("checked"),
      }
    }).done(res => {
      location.href = '/users/list';
    }).fail(err => {
      // console.error(err);
      alert(err.responseText);
    });
  }

  onKeyUp(e) {
    if (e.keyCode === 13) {
      loginPage.login();
    }
  }
}

const loginPage = new LoginPage();
$(document).ready(function() {
  loginPage.init();
});