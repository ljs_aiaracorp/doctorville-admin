class UserList {
  limit = 10;

  init() {
    this.loadPage(0);
  }

  loadPage(page) {
    $.ajax({
      url: '/ajax/users/list',
      method: 'get',
      data: {
        page: page,
        limit: this.limit,
      }
    }).done(res => {
      const temp = $("<div>");
      temp.html(res);
      $("#user-list").html(temp.find("#list tbody").html());
      $(".pagination").html(temp.find(".pagination").html());
    }).fail(err => {
      alert(err.responseText);
    });
  }

  gotoDetail(userId) {
    location.href = '/user/' + userId;
  }
}

const userList = new UserList();
$(document).ready(function() {
  userList.init();
});

function paginationClick(p) {
  userList.loadPage(p);
}