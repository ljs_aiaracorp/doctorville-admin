const express = require('express');
const router = express.Router();
const fs = require('fs');
const readChunk = require('read-chunk');
const fileType = require('file-type');
const path = require('path');
const im = require('imagemagick');
const sharp = require('sharp');

const AjaxHelper = require('../../classes/util/AjaxHelper');
const Common = require('../../classes/util/Common');
const Uploader = require('../../classes/util/Uploader');

const Api = require('../../classes/util/Api');

const ViewTemplate = require('../../classes/util/ViewTemplate');

const Admin = require('../../classes/model/Admin');

router.post('/login', async (req, res) => {
  const { email, password, saveInfo } = req.body;
  if (!email || email === "") {
    AjaxHelper.badRequest(res, '이메일을 입력해주세요.');
    return;
  }
  if (!password || password === "") {
    AjaxHelper.badRequest(res, '비밀번호를 입력해주세요.');
    return;
  }
  const result = await Api.login(email, password);
  // console.log(req.body);
  // console.log(result);
  if (result.error) {
    AjaxHelper.status(res, result.error.code, result.error.message);
    return;
  }
  if (saveInfo) {
    res.cookie('email', email);
    res.cookie('password', password);
  } else {
    res.clearCookie('email');
    res.clearCookie('password');
  }
  AjaxHelper.success(res, req.session.admin = new Admin(result));
});

router.post('/logout', async (req, res) => {
  req.session.admin = null;
  AjaxHelper.success(res, {});
});

router.get('/users/list', async (req, res) => {
  const { page, limit } = req.query;
  const result = await Api.getUsers(page, limit, req.session.admin.accessToken);
  // console.log(result);
  ViewTemplate.setAjaxReturn(res, 'users/listAjax', result, page, limit, result.count);
});

module.exports = router;
