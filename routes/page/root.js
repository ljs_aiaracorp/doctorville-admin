const express = require('express');
const router = express.Router();
const fs = require('fs');

const Common = require('../../classes/util/Common');
const ViewTemplate = require('../../classes/util/ViewTemplate');
const Api = require('../../classes/util/Api');

const User = require('../../classes/model/User');

const ExcelHelper = require('../../classes/util/ExcelHelper');
const excel = new ExcelHelper();

router.use('/', (req, res, next) => {
	// if (req.headers !== undefined && req.headers !== null && JSON.stringify(req.headers) !== '{}') console.info(req.headers);
	// if (req.params !== undefined && req.params !== null && JSON.stringify(req.params) !== '{}') console.info('req.params', req.params);
	// if (req.query !== undefined && req.query !== null && JSON.stringify(req.query) !== '{}') console.info('req.query', req.query);
	// if (req.body !== undefined && req.body !== null && JSON.stringify(req.body) !== '{}') console.info('req.body', req.body);

	// const protocol = req.headers['x-forwarded-proto'] || req.protocol;
	// if (protocol === 'http' && req.hostname !== 'localhost') {
	// 	res.redirect(`https://${req.hostname}${req.url}`);
	// 	return;
	// }

	if (req.url.indexOf('/ajax') > -1 ||
		req.url.indexOf('/favicon') > -1 ||
		req.url.indexOf('/download') > -1 ||
		req.url.indexOf('/uploads') > -1) {
		next();
		return;
	}
	if (req.url.indexOf('/login') > -1) {
		if (Api.checkLogin(req))
			res.redirect('/users/list');
		else
			next();
		return;
	}
	if (Api.checkLogin(req))
		next();
	else
		res.redirect('/login');
});

router.get('/', (req, res) => {
    res.redirect('/login');
});

router.get('/favicon.ico', (req, res) => {
	res.redirect('/images/logo.png');
});

router.get('/login', (req, res) => {
	const cookies = req.cookies;
	const email = cookies ? cookies.email : '';
	const password = cookies ? cookies.password : '';
	ViewTemplate.set(req, res, 'login', '', '', '', {
		email: email ? email : '',
		password: password ? password : '',
	});
});

router.get('/users/list', (req, res) => {
	ViewTemplate.set(req, res, 'users/list', '회원관리', 'users', 'list', {
		jsList: [
			'users/list.js'
		]
	});
});

router.get('/user/:userId', async (req, res) => {
	const { userId } = req.params;
	const result = await Api.getUser(userId, req.session.admin.accessToken);
	console.log(result);
	ViewTemplate.set(req, res, 'users/detail', '회원관리', 'users', 'list', {
		user: new User(result),
		jsList: [
			'users/detail.js'
		]
	});
});

module.exports = router;
